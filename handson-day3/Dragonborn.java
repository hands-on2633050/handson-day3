/**
 * Dragonborn
 */
public class Dragonborn implements Shoutable, Playable {

    private int level = 0;
    private String name;

    Dragonborn(String name){
        this.setName(name);
    }

    @Override
    public void shout() {
        System.out.println(wordOfPower);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public void levelUp() {
        this.level++;
    }

    @Override
    public double getWinChance() {
        // double chance = Math.max(0.6, this.getLevel() * 0.1);
        double chance = Math.min(0.9, 0.7 + (this.getLevel() * 0.05));

        return chance;
    }

    @Override
    public boolean checkWin() {
        double chance = Math.max(0.0, Math.min(1.0, this.getWinChance()));

        double randomValue = Math.random();

        return randomValue < chance;

    }

    @Override
    public String toString(){
        return "Level: " + this.getLevel() + "\nWin Chance: " + (this.getWinChance() * 100) + "%";
    }
    

    
}