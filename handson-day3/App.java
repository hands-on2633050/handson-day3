import java.util.Scanner;

/**
 * App
 */
public class App {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int points = 0;
        boolean lost = false;
        String gameName = String.format(" ___________                  .__         \r\n" + //
                    " /   _____/  | _____.__._______|__| _____  \r\n" + //
                    " \\_____  \\|  |/ <   |  |\\_  __ \\  |/     \\ \r\n" + //
                    " /        \\    < \\___  | |  | \\/  |  Y Y  \\\r\n" + //
                    "/_______  /__|_ \\/ ____| |__|  |__|__|_|  /\r\n" + //
                    "        \\/     \\/\\/                     \\/ ");

        System.out.println(gameName);

        System.out.print("What is your name? ");
        String name = scanner.nextLine();
        System.out.println("\n");

        Dragonborn player = new Dragonborn(name);
        Dragon dragon = new Dragon();

        String answer = "";

        System.out.println(player.getName() + " vs. Dragon");
        load();
        System.out.println(player);
        load();

        while (!answer.toLowerCase().equals("q")) {

            System.out.println("Fight!");
            load();
            System.out.printf("%s: ", player.getName());
            player.shout();

            System.out.print("Dragon: ");
            dragon.shout();

            load();

            boolean playerWon = player.checkWin();

            if(playerWon){
                System.out.println("Your shout wins over the dragon's!");
                load();
                boolean dragonDodged = dragon.attemptDodge();

                if(dragonDodged){
                    System.out.println("However, the dragon managed to dodge your attack!");
                }else {
                    player.levelUp();
                    System.out.println("LEVEL UP!");
                    load();
                    // System.out.println("Level: " + player.getLevel());
                    // System.out.println("Current win chance: " + (player.getWinChance() * 100) + "%");
                    System.out.println(player);
                    System.out.println("\n");
                }
            }else {
                System.out.println("You lost!");
                lost = true;
                break;
            }

            System.out.println("Would you like to quit? (q)");
            answer = scanner.nextLine();
        }

        points = lost ? 0 : player.getLevel() * 1000;

        System.out.println("Final points: " + points);

        scanner.close();
    }

    public static void load(){
        
        try {
          for(int i = 0; i <= 10; i++){
            Thread.sleep(100);
            System.out.print(". ");
        }  
        } catch (InterruptedException e) {
            System.out.println("error");
        }finally {
            System.out.println("\n");
        }
    }
}

