
/**
 * Shoutable
 */
public interface Shoutable {

    final String wordOfPower = "Fus Ro Dah!";

    public abstract void shout();
    
}