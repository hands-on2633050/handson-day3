/**
 * Dragon
 */
public class Dragon implements Shoutable, Flyable {

    @Override
    public void shout() {
        System.out.println(wordOfPower);
    }

    @Override
    public boolean attemptDodge() {
        
        double chance = Math.max(0.0, Math.min(1.0, dodgeChance));

        double randomValue = Math.random();

        return randomValue < chance;
    }
    
}