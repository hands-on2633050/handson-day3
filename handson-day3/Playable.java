/**
 * Playable
 */
public interface Playable {

    public abstract String getName();
    public abstract void setName(String name);
    public abstract int getLevel();
    public abstract void levelUp();
    public abstract double getWinChance();
    public abstract boolean checkWin();
    public abstract String toString();

}