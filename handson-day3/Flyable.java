/**
 * Flyable
 */
public interface Flyable {
    final double dodgeChance = 0.1;

    public abstract boolean attemptDodge();
    
}